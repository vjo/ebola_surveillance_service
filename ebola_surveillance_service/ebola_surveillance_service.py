from flask import Flask, make_response, request
from flask_htpasswd import HtPasswdAuth

from werkzeug.middleware.proxy_fix import ProxyFix

from json import dumps as json_dump

from os import environ
from sys import setswitchinterval

from sqlalchemy.dialects.postgresql import JSONB
from sqlalchemy.dialects.postgresql import UUID

from sqlalchemy import create_engine as sa_create_engine
from sqlalchemy import text as sa_text
from sqlalchemy import Column, Identity, Integer, String
from sqlalchemy import DateTime, ForeignKey

from sqlalchemy.exc import IntegrityError
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

from datetime import datetime, timezone
from uuid import uuid4
from ssl import create_default_context as ssl_create_context
from traceback import format_exc

# Set the thread switch interval to 25 microseconds
setswitchinterval(0.000025)

# SQLAlchemy boilerplate
Base = declarative_base()

# Database access parameters
pg_username = environ.get("PG_USERNAME")
pg_password = environ.get("PG_PASSWORD")
pg_hostname = environ.get("PG_HOSTNAME")
pg_port = environ.get("PG_PORT")
pg_db = environ.get("PG_DATABASE")

pg_connection_string_fmt = (
    'postgresql+pg8000://{username}:{password}@' +
    '{hostname}:{port}/{database}'
)
pg_url = pg_connection_string_fmt.format(
    username=pg_username, password=pg_password,
    hostname=pg_hostname, port=pg_port,
    database=pg_db
)

# Create SqlAlchemy engine for Postgres, with SSL,
# and a sessionmaker
pg_ssl_context = ssl_create_context()
pg_engine = sa_create_engine(pg_url,
                             client_encoding='utf8',
                             connect_args={"ssl_context": pg_ssl_context},
                             pool_size=10,
                             max_overflow=0,
                             pool_recycle=600,
                             pool_pre_ping=True,
                             pool_use_lifo=True,
                             echo=False)
pg_Session = sessionmaker(bind=pg_engine)

# Define the tables we'll be using in Postgres to record what
# comes from Kuali Build.
class EbolaSurveillanceJSON(Base):
    __tablename__ = 'ebola_surveillance_json'
    duid = Column(String(15), index=True)
    txn_id = Column(UUID(as_uuid=True), index=True, primary_key=True)
    source_json = Column(JSONB)

    def __repr__(self):
        return "<EbolaSurveillanceJSON(txn_id='%s', source_json='%s')>" % (
            self.txn_id, json_dump(self.source_json))


class EbolaSurveillance(Base):
    __tablename__ = 'ebola_surveillance'
    tbl_id = Column(Integer, Identity(), unique=True, primary_key=True)
    duid = Column(String(15), index=True)
    txn_id = Column(UUID(as_uuid=True),
                    ForeignKey('ebola_surveillance_json.txn_id'),
                    index=True)
    txn_date = Column(DateTime)

    def __repr__(self):
        return ("<EbolaSurveillance(tbl_id='%s', " +
                "duid='%s', txn_id='%s', txn_date='%s')>") % (
                    self.tbl_id, self.duid, self.txn_id, self.txn_date)


# Set up tables, if not already present.
while True:
    try:
        Base.metadata.create_all(pg_engine)
    except IntegrityError:
        continue
    except SystemExit:
        print('System exit forced while in table setup loop!')
        print('Investigate database connectivity.')
        break
    except:
        print('An unexpected error occurred while trying to set up tables!')
        raise
    else:
        break

# Body of COVID vaccination data proxy app
app = Flask(__name__)
app.config['FLASK_HTPASSWD_PATH'] = '/opt/ebola_surveillance_service/credentials/htpasswd'
app.config['FLASK_AUTH_ALL'] = True
app.wsgi_app = ProxyFix(app.wsgi_app, x_proto=1)

htpasswd = HtPasswdAuth(app)

def put_pg_db_data(duid, source_json):
    retval = False

    session = None
    session_error = False
    try:
        session = pg_Session()
        if session:
            txn_id = uuid4()
            txn_date = datetime.now()

            json_record = EbolaSurveillanceJSON(duid=duid,
                                                txn_id=txn_id,
                                                source_json=source_json)
            session.add(json_record)
            session.commit()

            surveillance_record = EbolaSurveillance(duid=duid,
                                                    txn_id=txn_id,
                                                    txn_date=txn_date)
            session.add(surveillance_record)
            session.commit()

            retval = True
        else:
            print('Unable to get connection to backend Postgres DB.')
    except Exception as e:
        print(f'Encountered a problem communicating '
              f'with backend Postgres DB: '
              f'{str(e)}')
        print('Exception backtrace follows:')
        print(format_exc())
        session_error = True
        
    if session_error:
        try:
            # Do our best to revert the transaction,
            # in the face of an error.
            session.rollback()
        except Exception as e:
            print(f'Encountered a problem performing rollback '
                  f'on backend Postgres DB: '
                  f'{str(e)}')
            print('Exception backtrace follows:')
            print(format_exc())

    if session:
        try:
            session.close()
        except Exception as e:
            print(f'Encountered a problem closing connection '
                  f'with backend Postgres DB: '
                  f'{str(e)}')
            print('Exception backtrace follows:')
            print(format_exc())

    return retval


@app.route('/api/v1/ebola_surveillance_submit', methods=['POST'])
def submitted_ebola_surveillance():
    if request.method != 'POST':
        return make_response('Malformed request.\n', 400)

    form_data = request.json

    duid = None
    try:
        duid = form_data['meta']['submittedBy']['schoolId']
    except:
        print('Some fields missing from JSON provided by Kuali!')

    insert_success = None
    if duid:
        insert_success = put_pg_db_data(duid, form_data)

    if insert_success:
        return make_response('Update accepted.\n', 200)

    return make_response('Malformed data.\n', 400)


if __name__ == '__main__':
    app.run(host='0.0.0.0')
